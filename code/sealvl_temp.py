#!/usr/bin/env python3

# import packages
import pandas as pd # For reading and manipulating 2D data (like spreadsheets)
import numpy as np # For doing numerical calculations (literally NUMerical PYthon)
import matplotlib.pyplot as plt # For making graphs
import matplotlib.dates as mdates
from sklearn.metrics import r2_score
from scipy.stats import pearsonr

url_clim = 'https://raw.githubusercontent.com/janzika/MATH3041/main/data/climate-change_2.csv'
SL_data = pd.read_csv(url_clim)
print(SL_data)

#
filtered_data = SL_data[(SL_data['Entity'] == 'World') & (SL_data['Average'].notna())][['Entity', 'Date', 'Average', 'Temperature anomaly']]
print(filtered_data)

#
filtered_data['Date'] = pd.to_datetime(filtered_data['Date'])
average_data = filtered_data.groupby(filtered_data['Date'].dt.year)[['Average', 'Temperature anomaly']].mean()
average_data = average_data.reset_index()
print(average_data)

# rate of change
average_data.set_index('Date', inplace=True)
average_data['Rate of change'] = average_data['Average'].diff()
average_data.reset_index(inplace=True)
print(average_data)


#
#plt.plot(average_data['Date'], average_data['Average'], 'ro', markersize=3, label='Data Points')
#plt.plot(average_data['Date'], average_data['Temperature anomaly'], 'ro', markersize=3, label='Data Points')
plt.plot(average_data['Temperature anomaly'], average_data['Rate of change'], 'ro', markersize=3)
#plt.plot(average_data['Date'], average_data['Rate of change'], 'ro', markersize=3)

plt.xlabel('Temperature Anomaly (°C)')
plt.ylabel('Rate of change in sea level (mm/yr)')
plt.title('Correlation of yearly temperature anomalies and change in sea level')
#plt.xlabel('Year')
#plt.ylabel('Global Sea Level Rise (mm)')
#plt.title('Trend of sea level rise from 1880-2020')
#plt.title('Trend of temperature anomalies from 1880-2020')
#plt.ylabel('Temperature Anomaly (°C)') 


# Calculate and plot the line of best fit
#x = average_data['Date'][1:-1]
x = average_data['Temperature anomaly'][1:-1]
y = average_data['Rate of change'][1:-1]
#y = average_data['Average']
#y = average_data['Temperature anomaly']

#coefficients = np.polyfit(x, np.log(y + 200), 3)  # Apply logarithmic transformation to y
coefficients = np.polyfit(x, y, 1)
poly_eq = np.poly1d(coefficients)
plt.plot(x, poly_eq(x))
#plt.plot(x, np.exp(poly_eq(x)) - 200, label='Line of Best Fit')

plt.show()

y_pred = poly_eq(x)  # Predicted y values from the line of best fit
#r_squared = r2_score(y, y_pred)
#print(r_squared)

correlation, pval = pearsonr(y, y_pred)
print(correlation, pval)
