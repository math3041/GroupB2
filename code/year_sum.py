#!/usr/bin/env python3

# import packages
import pandas as pd 
import numpy as np 
import matplotlib.pyplot as plt 
import matplotlib.dates as mdates
from sklearn.metrics import r2_score
from scipy.stats import pearsonr

url_clim = 'https://raw.githubusercontent.com/janzika/MATH3041/main/data/climate-change_2.csv'
SL_data = pd.read_csv(url_clim)
print(SL_data)

url_co2 = 'https://raw.githubusercontent.com/janzika/MATH3041/main/data/annual-co-emissions-by-region.csv'
co2_data = pd.read_csv(url_co2)
print(co2_data)

#
filtered_data = SL_data[(SL_data['Entity'] == 'World') & (SL_data['Average'].notna())][['Entity', 'Date', 'Average', 'Temperature anomaly']]

co_filtered_data = co2_data[(co2_data['Entity'] == 'World') & (co2_data['Year'] >= 1880)]

#
filtered_data['Date'] = pd.to_datetime(filtered_data['Date'])
average_data = filtered_data.groupby(filtered_data['Date'].dt.year)[['Average', 'Temperature anomaly']].mean()
average_data = average_data.reset_index()

# rate of change
average_data.set_index('Date', inplace=True)
average_data['Rate of change'] = average_data['Average'].diff()
average_data.reset_index(inplace=True)

    # Merge average_data and co_filtered_data based on the 'Date' column
co_filtered_data.rename(columns={'Year' : 'Date'}, inplace=True)

merged_data = pd.merge(average_data, co_filtered_data, on='Date')

# Create a new column with the cumulative sum
merged_data['Cumulative co2 emissions'] = merged_data['Annual CO₂ emissions (zero filled)'].cumsum()



# Plot cumulative co2 and year
plt.plot(merged_data['Date'], merged_data['Cumulative co2 emissions'], 'ro', markersize=3, label='Data Points')
plt.xlabel('Year')
plt.ylabel('Cumulative CO₂ emissions (10 billion tonnes)')
plt.title('Cumulative CO₂ Emissions since 1880')

x = merged_data['Date']
y = (merged_data['Cumulative co2 emissions'])
coefficients = np.polyfit(x, y, 3)
poly_eq = np.poly1d(coefficients)
plt.plot(x, poly_eq(x))

x_fit = np.linspace(min(x), max(x) + 100, 100)  # Extend the range of 'x' for projections
plt.plot(x_fit, (poly_eq(x_fit)))

#correlation, pval = pearsonr(y, poly_eq(x))
#print(correlation, pval)
r_squared = r2_score(y, poly_eq(x))
print(r_squared)

plt.show()

print(coefficients)

